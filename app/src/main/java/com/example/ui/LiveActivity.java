package com.example.ui;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LiveActivity extends Activity implements CameraBridgeViewBase.CvCameraViewListener2{
    private static final String TAG="MainActivity";

    private Mat mRgba;
    private Mat mGray;
    private CameraBridgeViewBase mOpenCvCameraView;

    private ImageView flip_camera;
    private int mCameraId = 0; // 0:후면 카메라 1:전면카메라

    private MediaRecorder recorder;
    private ImageView video_button;
    private int take_video_or_not = 0; // 1:start recording 0:stop recoring

    private CascadeClassifier cascadeClassifier;

    private BaseLoaderCallback mLoaderCallback =new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status){
                case LoaderCallbackInterface
                        .SUCCESS:{
                    Log.i(TAG,"OpenCv Is loaded");
                    mOpenCvCameraView.enableView();
                }
                default:
                {
                    super.onManagerConnected(status);

                }
                break;
            }
        }
    };

    public LiveActivity(){
        Log.i(TAG,"Instantiated new "+this.getClass());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        int MY_PERMISSIONS_REQUEST_CAMERA=0;
        // if camera permission is not given it will ask for it on device
        if (ContextCompat.checkSelfPermission(LiveActivity.this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED){
            ActivityCompat.requestPermissions(LiveActivity.this, new String[] {Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);
        }
        // this will allow you app to ask your permission before writing
        if (ContextCompat.checkSelfPermission(LiveActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_DENIED){
            ActivityCompat.requestPermissions(LiveActivity.this, new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_CAMERA);
        }
        if (ContextCompat.checkSelfPermission(LiveActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_DENIED){
            ActivityCompat.requestPermissions(LiveActivity.this, new String[] {Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_CAMERA);
        }
        // ask for record audio permission
        if (ContextCompat.checkSelfPermission(LiveActivity.this, Manifest.permission.RECORD_AUDIO)
                == PackageManager.PERMISSION_DENIED){
            ActivityCompat.requestPermissions(LiveActivity.this, new String[] {Manifest.permission.RECORD_AUDIO}, MY_PERMISSIONS_REQUEST_CAMERA);
        }

        setContentView(R.layout.live);

        mOpenCvCameraView=(CameraBridgeViewBase) findViewById(R.id.frame_Surface);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);

        // 카메라 전환
        flip_camera = findViewById(R.id.flip_camera);
        // when flip camera button is clicked
        flip_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swapCamera();
            }
        });

        // 영상 녹화
        recorder = new MediaRecorder();

        video_button = findViewById(R.id.video_button);
        video_button.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(take_video_or_not == 0) {
                        // start recording
                        // craete folder ImagePro
                        try {
                            File folder = new File(Environment.getExternalStorageDirectory().getPath()+ "/ImagePro");
                            // if not create a new folder
                            boolean success = true;
                            if (!folder.exists()) {
                                success = folder.mkdir();
                            }
                            video_button.setImageResource(R.drawable.red_circle);
                            recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                            recorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);
                            CamcorderProfile camcorderProfile = CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH);
                            recorder.setProfile(camcorderProfile);

                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
                            String current_Date_and_Time = sdf.format(new Date());
                            String filename = Environment.getExternalStorageDirectory().getPath()
                                    + "/ImagePro/" + current_Date_and_Time + ".mp4";
                            recorder.setOutputFile(filename);
                            recorder.setVideoSize(480, 720);
                            // default size
                            // start recorder
                            recorder.prepare();
                            mOpenCvCameraView.setRecorder(recorder);
                            recorder.start();
                        }
                        catch (IOException e) {
                            e.printStackTrace();
                        }
                        take_video_or_not = 1;
                    }
                    else {
                        video_button.setImageResource(R.drawable.circle);
                        mOpenCvCameraView.setRecorder(null);
                        recorder.stop();
                        try {
                            Thread.sleep(1000);
                        }
                        catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                        take_video_or_not = 0;
                    }
                }
                return true;
            }
        });

        // 얼굴 검출위한 파일 불러오기
        //load the model
        try {
            InputStream is = getResources().openRawResource(R.raw.haarcascade_frontalface_alt);
            File cascadeDir = getDir("cascade", Context.MODE_PRIVATE); //creating a folder
            File mCascadeFile = new File(cascadeDir, "haarcascade_frontalface_alt.xml"); // creating file on that folder
            FileOutputStream os = new FileOutputStream(mCascadeFile);
            byte[] buffer = new byte[4096];
            int byteRead;
            // writing that file from raw folder
            while((byteRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, byteRead);
            }
            is.close();
            os.close();

            // loading file from cascade folder created above
            cascadeClassifier = new CascadeClassifier(mCascadeFile.getAbsolutePath());
            // model is loaded
        }
        catch(IOException e){
            Log.i(TAG, "Cascade file not found");
        }
    }

    private void swapCamera() {
        mCameraId = mCameraId ^ 1; //basic not operation
        // disable current cameraview
        mOpenCvCameraView.disableView();

        // setCameraIndex
        mOpenCvCameraView.setCameraIndex(mCameraId);
        // enable view
        mOpenCvCameraView.enableView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (OpenCVLoader.initDebug()){
            //if load success
            Log.d(TAG,"Opencv initialization is done");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
        else{
            //if not loaded
            Log.d(TAG,"Opencv is not loaded. try again");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_4_0,this,mLoaderCallback);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mOpenCvCameraView !=null){
            mOpenCvCameraView.disableView();
        }
    }

    public void onDestroy(){
        super.onDestroy();
        if(mOpenCvCameraView !=null){
            mOpenCvCameraView.disableView();
        }

    }

    public void onCameraViewStarted(int width ,int height){
        mRgba=new Mat(height,width, CvType.CV_8UC4);
        mGray =new Mat(height,width,CvType.CV_8UC1);
    }
    public void onCameraViewStopped(){
        mRgba.release();
    }

    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        mRgba = inputFrame.rgba();
        mGray = inputFrame.gray();

        if(mCameraId == 1) {
            Core.flip(mRgba, mRgba, -1);
            Core.flip(mGray, mGray, -1);
        }

        // in precessing pass mRgba to cascaderec class
        mRgba = CascadeRec(mRgba);

        return mRgba;
    }

    private Mat CascadeRec(Mat mRgba) { // 얼굴 검출
        // original frame is -90 degree so we have to rotate is to 90 to get proper to face for detection
        Mat a=mRgba.t();
        Core.flip(a,mRgba,1);
        a.release();

        // convert it into RGB
        Mat mRbg = new Mat();
        Imgproc.cvtColor(mRgba, mRbg, Imgproc.COLOR_RGBA2RGB);

        int height = mRbg.height();
        int width = mRbg.width();

        // minimum size of face in frame
        int absoluteFaceSize = (int)(height * 0.1);

        // face detection using opencv + haarcascade model
        MatOfRect faces = new MatOfRect();

        if(cascadeClassifier != null) {
            // input output
            cascadeClassifier.detectMultiScale(mRbg, faces, 1.1, 2, 2
                    ,new Size(absoluteFaceSize, absoluteFaceSize),  new Size()); //  minimum size of output
        }

        //loop through all faces
        Rect[] facesArray = faces.toArray();

        for (int i = 0; i < facesArray.length; i++) {
            // draw face on original frame mRgba
            Imgproc.rectangle(mRgba, facesArray[i].tl(), facesArray[i].br(), new Scalar(0, 255, 0, 255), 2);

            Mat blurMat = mRgba.submat((int) facesArray[i].tl().y, (int) facesArray[i].br().y, (int) facesArray[i].tl().x, (int) facesArray[i].br().x);
            Imgproc.blur(blurMat, blurMat, new Size(99, 99));

        }

        // rotate back original frame to -90 degree
        Mat b=mRgba.t();
        Core.flip(b,mRgba,0);
        b.release();

        return mRgba;
    }

}